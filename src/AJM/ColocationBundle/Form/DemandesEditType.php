<?php

namespace AJM\ColocationBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;


class DemandesEditType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('message',   TextareaType::class, array('label'=>'Message','required' => false))
                ->remove('envoyer',      SubmitType::class)
                ->add('Modifier', SubmitType::class, array('label'=>'bouton.modifier.demande'));
    }
    
    public function getParent(){
        return DemandesType::class;
    }

}
