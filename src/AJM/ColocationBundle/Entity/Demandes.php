<?php

namespace AJM\ColocationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Demandes
 *
 * @ORM\Table(name="demandes")
 * @ORM\Entity(repositoryClass="AJM\ColocationBundle\Repository\DemandesRepository")
 */
class Demandes
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="message", type="text", nullable=true)
     */
    private $message;

    /**
     * @var string
     *
     * @ORM\Column(name="etat", type="string", length=255, nullable=true)
     */
    private $etat;

    /**
     * @ORM\ManyToOne(targetEntity="AJM\ColocationBundle\Entity\Colocation", inversedBy="demandes")
     */
    private $colocation;

    /**
     * @ORM\ManyToOne(targetEntity="AJM\UserBundle\Entity\User", inversedBy="demandes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set message
     *
     * @param string $message
     *
     * @return Demandes
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set etat
     *
     * @param string $etat
     *
     * @return Demandes
     */
    public function setEtat($etat)
    {
        $this->etat = $etat;

        return $this;
    }

    /**
     * Get etat
     *
     * @return string
     */
    public function getEtat()
    {
        return $this->etat;
    }

    /**
     * Set colocation
     *
     * @param \AJM\ColocationBundle\Entity\Colocation $colocation
     *
     * @return Demandes
     */
    public function setColocation(\AJM\ColocationBundle\Entity\Colocation $colocation = null)
    {
        $this->colocation = $colocation;

        return $this;
    }

    /**
     * Get colocation
     *
     * @return \AJM\ColocationBundle\Entity\Colocation
     */
    public function getColocation()
    {
        return $this->colocation;
    }

    /**
     * Set user
     *
     * @param \AJM\UserBundle\Entity\User $user
     *
     * @return Demandes
     */
    public function setUser(\AJM\UserBundle\Entity\User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AJM\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    public function __toString() {
        return $this->id;
    }
}
