<?php

namespace AJM\ColocationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Colocation
 *
 * @ORM\Table(name="colocation")
 * @ORM\Entity(repositoryClass="AJM\ColocationBundle\Repository\ColocationRepository")
 */
class Colocation
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateDebut", type="datetime")
     */
    private $dateDebut;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateFin", type="datetime", nullable=true)
     */
    private $dateFin;

    /**
     * @var string
     *
     * @ORM\Column(name="titreAnnonce", type="string", length=255)
     */
    private $titreAnnonce;

    /**
     * @var string
     *
     * @ORM\Column(name="descAnnonce", type="text", nullable=true)
     */
    private $descAnnonce;

    /**
     * @var int
     *
     * @ORM\Column(name="nbPlaces", type="integer")
     */
    private $nbPlaces;

    /**
     * @var int
     *
     * @ORM\Column(name="loyer", type="integer")
     */
    private $loyer;

    /**
     * @ORM\OneToOne(targetEntity="AJM\ColocationBundle\Entity\Logement", cascade={"persist", "remove"})
     */
    private $logement;

    /**
     * @ORM\ManyToOne(targetEntity="AJM\UserBundle\Entity\User", inversedBy="colocations")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;    

    /**
     * @ORM\OneToMany(targetEntity="AJM\ColocationBundle\Entity\Demandes", mappedBy="colocation", cascade={"persist", "remove"})
     */
    private $demandes;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dateDebut
     *
     * @param \DateTime $dateDebut
     *
     * @return Colocation
     */
    public function setDateDebut($dateDebut)
    {
        $this->dateDebut = $dateDebut;

        return $this;
    }

    /**
     * Get dateDebut
     *
     * @return \DateTime
     */
    public function getDateDebut()
    {
        return $this->dateDebut;
    }

    /**
     * Set dateFin
     *
     * @param \DateTime $dateFin
     *
     * @return Colocation
     */
    public function setDateFin($dateFin)
    {
        $this->dateFin = $dateFin;

        return $this;
    }

    /**
     * Get dateFin
     *
     * @return \DateTime
     */
    public function getDateFin()
    {
        return $this->dateFin;
    }

    /**
     * Set titreAnnonce
     *
     * @param string $titreAnnonce
     *
     * @return Colocation
     */
    public function setTitreAnnonce($titreAnnonce)
    {
        $this->titreAnnonce = $titreAnnonce;

        return $this;
    }

    /**
     * Get titreAnnonce
     *
     * @return string
     */
    public function getTitreAnnonce()
    {
        return $this->titreAnnonce;
    }

    /**
     * Set descAnnonce
     *
     * @param string $descAnnonce
     *
     * @return Colocation
     */
    public function setDescAnnonce($descAnnonce)
    {
        $this->descAnnonce = $descAnnonce;

        return $this;
    }

    /**
     * Get descAnnonce
     *
     * @return string
     */
    public function getDescAnnonce()
    {
        return $this->descAnnonce;
    }

    /**
     * Set nbPlaces
     *
     * @param integer $nbPlaces
     *
     * @return Colocation
     */
    public function setNbPlaces($nbPlaces)
    {
        $this->nbPlaces = $nbPlaces;

        return $this;
    }

    /**
     * Get nbPlaces
     *
     * @return int
     */
    public function getNbPlaces()
    {
        return $this->nbPlaces;
    }

    /**
     * Set loyer
     *
     * @param integer $loyer
     *
     * @return Colocation
     */
    public function setLoyer($loyer)
    {
        $this->loyer = $loyer;

        return $this;
    }

    /**
     * Get loyer
     *
     * @return int
     */
    public function getLoyer()
    {
        return $this->loyer;
    }

    /**
     * Set logement
     *
     * @param \AJM\ColocationBundle\Entity\Logement $logement
     *
     * @return Colocation
     */
    public function setLogement(\AJM\ColocationBundle\Entity\Logement $logement = null)
    {
        $this->logement = $logement;

        return $this;
    }

    /**
     * Get logement
     *
     * @return \AJM\ColocationBundle\Entity\Logement
     */
    public function getLogement()
    {
        return $this->logement;
    }

    /**
     * Set user
     *
     * @param \AJM\UserBundle\Entity\User $user
     *
     * @return Colocation
     */
    public function setUser(\AJM\UserBundle\Entity\User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AJM\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->demandes = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add demande
     *
     * @param \AJM\ColocationBundle\Entity\Demandes $demande
     *
     * @return Colocation
     */
    public function addDemande(\AJM\ColocationBundle\Entity\Demandes $demande)
    {
        $this->demandes[] = $demande;

        return $this;
    }

    /**
     * Remove demande
     *
     * @param \AJM\ColocationBundle\Entity\Demandes $demande
     */
    public function removeDemande(\AJM\ColocationBundle\Entity\Demandes $demande)
    {
        $this->demandes->removeElement($demande);
    }

    /**
     * Get demandes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDemandes()
    {
        return $this->demandes;
    }
}
