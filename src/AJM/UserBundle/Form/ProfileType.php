<?php


namespace AJM\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class ProfileType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('nom', TextType::class, array('label' => 'inscription.nom'))
                ->add('prenom', TextType::class, array('label' => 'inscription.prenom'))
                ->add('telephone', TextType::class, array('label' => 'inscription.telephone'));
    }

    public function getParent()
    {
        return 'FOS\UserBundle\Form\Type\ProfileFormType';
    }

    public function getBlockPrefix()
 
    {
        return 'ajm_user_registration';
    }
  
    public function getName()
  
    {
        return $this->getBlockPrefix();
    }
}