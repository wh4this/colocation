# Guide de déploiement - KoloK

## Etape 1 :
Cloner l’application avec Git à l’adresse https://github.com/wh4this/colocation dans le dossier servis par votre serveur web (PHP 5 minimum https://secure.php.net/downloads.php et un serveur MySQL https://dev.mysql.com/downloads/mysql/ doivent aussi être installés et configurés pour le serveur web) :
    `git clone https://github.com/wh4this/colocation`

## Etape 2 : 
Installer composer si ce n’est pas déjà fait à l’adresse https://getcomposer.org/doc/00-intro.md

## Etape 3 :
Se rendre à la racine du dossier précédemment cloné (colocation) puis effectuer la commande dans votre terminal :
    `composer update`
Cette commande installe toutes les dépendances de l’application.

## Etape 4 : 
Après avoir installé les dépendances, se rendre à la racine du dossier colocation puis effectuer la commande dans votre terminal :
    `php bin/console doctrine:database:create`
    `php bin/console doctrine:schema:update --force`
Pour créer la base de données.

## Etape 5 :
Vous êtes maintenant prêt à utiliser l’application ! Vous pouvez y accéder à l’adresse :
    `adresse-de-votre-serveur/colocation/web`

## Information
Si la base de données a été mal configurée, vous pouvez toujours éditer le fichier se situant à l’endroit suivant : 
    `colocation/app/config/parameters.yml`
Pour devenir administrateur de l’application, créer un utilisateur sur l'application puis executer la commande suivante dans le répartoire racine de l'application :
    `php bin/console fos:user:promote nom_de_l_utilisateur ROLE_ADMIN`
